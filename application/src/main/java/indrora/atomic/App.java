package indrora.atomic;

import indrora.atomic.db.Database;
import indrora.atomic.model.Authentication;
import indrora.atomic.model.ColorScheme;
import indrora.atomic.model.ColorSchemeManager;
import indrora.atomic.model.Identity;
import indrora.atomic.model.Server;
import indrora.atomic.model.Settings;
import indrora.atomic.model.Status;
import indrora.atomic.utils.LatchingValue;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.Arrays;

import static indrora.atomic.model.Settings.*;

public class App extends Application {

  Atomic atomic;

  public App() {
    super();

    autoconnectComplete = new LatchingValue<Boolean>(true, false);
  }

  private static LatchingValue<Boolean> autoconnectComplete;

  private static Settings _s;

  private static Context _ctx;

  public static Context getAppContext() {
    return _ctx;
  }


    private static ColorSchemeManager _csMgr;
  public static ColorScheme getColorScheme() {
    return new ColorScheme(_s.getColorScheme(), _s.getUseDarkColors());
  }


  public static Settings getSettings() {
    if(_s == null) {
      _s = new Settings(getAppContext());
    }
    return _s;
  }

  public static Boolean doAutoconnect() {
    return autoconnectComplete.getValue();
  }

  private static Resources _r;

  public static Resources getSResources() {
    return _r;
  }

  private void addTsSupportServer() {
    /* Add tssupport.org server info */
    Database db = new Database(this);
    Authentication a = new Authentication();
    ArrayList<String> channels = new ArrayList<String>(Arrays.asList("#lobby", "#tssupport_app"));

    Identity identity = new Identity();
    identity.setNickname("tssupport_" + Settings.getRandomNick(5));
    identity.setUsername("tssupport_app");
    identity.setRealName("TransAdvice.org Chat App");

    long identityId = db.addIdentity(identity.getNickname(),
                                     identity.getIdent(),
                                     identity.getRealName(),
                                     identity.getAliases());

    Server server = new Server();
    server.setHost("ranma.ftee.org");
    server.setPort(6697);
    server.setPassword("");
    server.setTitle("TransAdvice Chat");
    server.setCharset("UTF-8");
    server.setUseSSL(true);
    server.setAutoconnect(true);
    server.setStatus(Status.DISCONNECTED);
    server.setAuthentication(a);

    long serverId = db.addServer(server, (int)identityId);

    db.setChannels((int)serverId, channels);

    db.close();
  }

  @Override
  public void onCreate() {
    // Context exists here.

    _ctx = getApplicationContext();

    Atomic.getInstance().loadServers(_ctx);


    indrora.atomic.model.Settings _settings = new Settings(this);
    _s = _settings;
    // Release 16 changes things for colors.
    // This is a much more elegant solution than I had here. Be glad.
    if( _s.getLastRunVersion() < 16 ) {
      _settings.setColorScheme("default");
    }

    _r = getResources();

    /*
    _csMgr = new ColorSchemeManager();

    PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(_csMgr);
*/


    if( _settings.getCurrentVersion() > _settings.getLastRunVersion() ) {
      addTsSupportServer();
      Intent runIntent = new Intent(this, FirstRunActivity.class);
      runIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      this.startActivity(runIntent);
    }

    String ll = _settings.getDefaultNick();
    ll = ll.trim();

    super.onCreate();
  }
}
